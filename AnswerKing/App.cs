﻿namespace AnswerKing
{
    public class App
    {
        private int choice;
        private bool IsEmpty;
        private List<Item> ListOfItems = new List<Item>();
        /*int ID =  ListOfItems.Count();*/
        public void Run()
        {
            do
            {
                Console.WriteLine("\n");

                Console.WriteLine("-----------------");
                Console.WriteLine("|AK Admin portal|");
                Console.WriteLine("-----------------");

                Console.WriteLine("\n");

                Console.WriteLine("1.Create an Item");
                Console.WriteLine("2.Detail(s)");
                Console.WriteLine("3.Delete item");
                Console.WriteLine("4.Update item");
                Console.WriteLine("5.Close App");

                Console.WriteLine("\nInput:");

                if (Int32.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            Console.Clear();
                            ListOfItems.Add(AddItem());
                            StockCounter();
                            break;
                        case 2:
                            if (StockChecker())
                            {
                                ShowItems();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\nThe stock is currently empty!");
                            }
                            break;
                        case 3:
                            if (StockChecker())
                            {
                                DeleteAnItem();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\nThe stock is currently empty!");
                            }
                            break;
                        case 4:
                            if (StockChecker())
                            {
                                ShowItems();
                                EditAnItem();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\nThe stock is currently empty!");
                            }
                            break;
                        case 5:
                            Console.WriteLine("\nSee you later!\n");
                            Thread.Sleep(1500);
                            Environment.Exit(0);
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("\nInvalid input please try again!");
                            break;
                    }
                }
            } while (choice != 5);
        }

        public void StockCounter()
        {
            int NoOfItems = ListOfItems.Count();
            Console.WriteLine($"\nThere are {NoOfItems} item(s) in stock!");
        }

        public bool StockChecker()
        {
            IsEmpty = !ListOfItems.Any();
            if (IsEmpty) { return false; }
            else { return true; }
        }

        public Item AddItem()
        {
            int Stock, Calories;
            string? Name, Description;
            decimal Price;

            Console.WriteLine("----------------");
            Console.WriteLine("|Create an item|");
            Console.WriteLine("----------------");

            Console.WriteLine("\nPlease enter the 'Name' of the Item: ");
            Name = Console.ReadLine();

            while (string.IsNullOrEmpty(Name))
            {
                Console.WriteLine("\nName can't be empty! please try again: ");
                Name = Console.ReadLine();
            }

            Console.WriteLine("\nPlease enter the 'Price' of the Item: ");
            bool success = decimal.TryParse(Console.ReadLine(), out Price);

            while (!success)
            {
                Console.WriteLine("Invalid Price! please try again: ");
                success = decimal.TryParse(Console.ReadLine(), out Price);
            }

            Console.WriteLine("\nPlease enter the 'Description' of the Item: ");
            Description = Console.ReadLine();

            while (string.IsNullOrEmpty(Description))
            {
                Console.WriteLine("\nDescription can't be empty! please try again: ");
                Description = Console.ReadLine();
            }


            Console.WriteLine("\nHow much would you put in stock?: ");
            success = int.TryParse(Console.ReadLine(), out Stock);

            /*Console.WriteLine("Stock: ", Stock.ToString());
            Console.WriteLine("check: ", success.ToString());

            if (Stock.Equals("") ||  Stock.Equals(" "))
            {
                Stock = ListOfItems[ItemID].stock;
                success = true;
            }*/

            while (!success)
            {
                Console.WriteLine("Invalid input! please try again: ");
                success = int.TryParse(Console.ReadLine(), out Stock);
            }

            Console.WriteLine("\nHow much calories does this item have?: ");
            success = int.TryParse(Console.ReadLine(), out Calories);

            while (!success)
            {
                Console.WriteLine("\nInvalid input! please try again: ");
                success = int.TryParse(Console.ReadLine(), out Calories);
            }


            Item tempItem = new Item((ListOfItems.Count() + 1), Name, Price, Description, Stock, Calories);

            Console.Clear();

            Console.WriteLine("\nThank you! Redirecting you to the main menu...");
            Thread.Sleep(500);

            Console.Clear();

            return tempItem;
        }

        public void ShowItems()
        {
            Console.Clear();
            Console.WriteLine("-------");
            Console.WriteLine("|Stock|");
            Console.WriteLine("-------");

            foreach (var item in ListOfItems)
            {
                Console.WriteLine(
                    "\nItem ID: " + (ListOfItems.IndexOf(item) + 1)+ 
                    "\n" +
                    "\nName: " + item.name +
                    "\nPrice £" + item.price +
                    "\nDescription: " + item.description +
                    "\nStock count: " + item.stock +
                    "\nCalorie count: " + item.calories);
            }
        }

        public void DeleteAnItem()
        {
            Console.Clear();
            ShowItems();
            Console.WriteLine("\nEnter the item ID you want to delete:");

            bool success = int.TryParse(Console.ReadLine(), out int ItemNo);

            while (!success)
            {
                Console.WriteLine("Invalid input! please try again: ");
                success = int.TryParse(Console.ReadLine(), out ItemNo);
            }


            if (ItemNo > ListOfItems.Count() || ItemNo <= 0)
            {
                Console.Clear();
                Console.WriteLine("\nWoahhh that item does not exist buddy!");
            }
            else
            {
                ListOfItems.RemoveAt(ItemNo - 1);
                ShowItems();
            }
        }

        public void EditAnItem()
        {
            Item UpdatedItem;

            Console.Clear();
            ShowItems();
            Console.WriteLine("\nEnter the item no. you want to update:");

            bool success = int.TryParse(Console.ReadLine(), out int ItemNo);

            while (!success)
            {
                Console.WriteLine("Invalid input! please try again: ");
                success = int.TryParse(Console.ReadLine(), out ItemNo);
            }

            if (ItemNo > ListOfItems.Count() || ItemNo <= 0)
            {
                Console.WriteLine("\nWoahhh that item does not exist buddy!");
            }
            else
            {
                UpdatedItem = AddItem();
                ListOfItems[ItemNo - 1] = UpdatedItem; 
                ShowItems();
            }
        }
    }
}
