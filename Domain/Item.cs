﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Item
    { 
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;    
        public string Description { get; set; } = string.Empty;
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public int Calories { get; set; }

        public Item(int id, string name, string description, decimal price, int stock, int calories)
        {
            this.Id = id;
            this.Name = name;   
            this.Description = description; 
            this.Price = price; 
            this.Stock = stock; 
            this.Calories = calories;   
        }
        public Item()
        {

        }
    }
}
