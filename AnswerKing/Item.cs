﻿namespace AnswerKing
{
    public class Item
    {
        private int ID = 0;
        private string Name = "Default Item";
        private decimal Price = 0;
        private string Description = "Default Desc.";
        private int Stock = 0;
        private int Calories = 0;

        public Item()
        {

        }
        public Item(int ID, string Name, decimal Price, string Description, int Stock, int Calories)
        {
            this.ID = ID; //for now we can edit/create this
            this.Name = Name;
            this.Price = Price;
            this.Description = Description;
            this.Stock = Stock;
            this.Calories = Calories;
        }

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }
        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        public decimal price
        {
            get { return Price; }
            set { Price = value; }
        }
        public string description
        {
            get { return Description; }
            set { Description = value; }
        }
        public int stock
        {
            get { return Stock; }
            set { Stock = value; }
        }
        public int calories
        {
            get { return Calories; }
            set { Calories = value; }
        }
    }
}
