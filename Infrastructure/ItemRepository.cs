﻿using Service;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ItemRepository : IItemRepository
    {
        //mock data
        public static List<Item> items = new List<Item>()
        {
            new Item { Id = 1, Name = "King Burger",  Description = "Big n' Juicy",Price = 19.99m,  Stock = 100, Calories = 200},
            new Item { Id = 2, Name = "Queen Burger",  Description = "Super Big n' Juicy",Price = 29.99m,  Stock = 150, Calories = 350},
            new Item { Id = 3, Name = "Jolly Steak",  Description = "Mouth watering Wagyu steak",Price = 399.99m,  Stock = 200, Calories = 250},
            new Item { Id = 4, Name = "Rainbow Icecream",  Description = "Taste the rainbow!",Price = 6.99m,  Stock = 500, Calories = 500},
        };

        public async Task<Item> AddItem(Item item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Item is empty!");
            }

            items.Add(item);
            return item;
        }

        public async Task<Item> DeleteItem(int id)
        {
           var item = items.SingleOrDefault(i => i.Id == id);

            if (item == null)
            {
                throw new ArgumentNullException($"Item with the id of {id} does not exist!");
            }

            items.Remove(item);
            return item;
        }

        public async Task<List<Item>> GetAllItems()
        {
            return items;
        }

        public async Task<Item> GetAnItem(int id)
        {
            var result = items.SingleOrDefault(i => i.Id == id);
            return result;
        }

        public async Task<Item> UpdateItem(Item item)
        {
            var itemToUpdate = items.SingleOrDefault(i => i.Id == item.Id);

            if (itemToUpdate == null)
            {
                throw new ArgumentNullException("Item is empty!");
            }

            itemToUpdate.Id = item.Id;
            itemToUpdate.Name = item.Name;
            itemToUpdate.Description = item.Description;
            itemToUpdate.Price = item.Price;
            itemToUpdate.Stock = item.Stock;
            itemToUpdate.Calories = item.Calories;

            return itemToUpdate;
        }
    }
}
