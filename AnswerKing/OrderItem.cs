﻿namespace AnswerKing
{
    public class OrderItem
    {
        private Item ItemID;
        private Order OrderID;
        private int Quantity;

        public OrderItem(Item ItemID, Order OrderID, int Quantity)
        {
            this.ItemID = ItemID;
            this.OrderID = OrderID;
            this.Quantity = Quantity;
        }

        public int itemID
        {
            get { return ItemID.id; }
            set { ItemID.id = value; }
        }
        public int orderID
        {
            get { return OrderID.id; }
            set { OrderID.id = value; }
        }
        public int quantity
        {
            get { return Quantity; }
            set { Quantity = value; }
        }
    }
}
