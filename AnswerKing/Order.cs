﻿namespace AnswerKing
{
    public class Order
    {
        private int ID;
        private OrderStatus OrderStatus;
        private decimal Total;

        public Order(int ID, OrderStatus OrderStatus, decimal Total)
        {
            this.ID = ID;
            this.OrderStatus = OrderStatus;
            this.Total = Total;
        }

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }
        public OrderStatus orderStatus
        {
            get { return OrderStatus; }
            set { OrderStatus = value; }
        }

        public decimal total
        {
            get { return Total; }
            set { Total = value; }
        }
    }
}
