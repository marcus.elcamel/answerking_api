﻿namespace AnswerKing
{
    public class OrderStatus
    {
        private int ID;
        private List<string> Status = new List<string> { "Approved", "Declined", "Pending" };
        public OrderStatus(int ID, List<string> Status)
        {
            this.ID = ID;
            this.Status = Status;
        }

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }
        public List<string> status
        {
            get { return Status; }
            set { Status = value; }
        }
    }
}
