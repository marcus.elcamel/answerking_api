﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IItemRepository
    {
        public Task<Item> AddItem(Item item);
        public Task<List<Item>> GetAllItems();
        public Task<Item> GetAnItem(int id);
        public Task<Item> UpdateItem(Item item);
        public Task<Item> DeleteItem(int id);

    }
}
