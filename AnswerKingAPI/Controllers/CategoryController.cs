﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Service;

namespace AnswerKingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService CategoryService;
        public CategoryController(ICategoryService categoryService)
        {
            this.CategoryService = categoryService;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllCategories()
        {
            try
            {
                var categories = await this.CategoryService.GetAllCategories();

                return this.Ok(categories);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error getting data from the database!");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddCategory(Category category)
        {
            try
            {
                var categoryToAdd = await this.CategoryService.AddCategory(category);

                if (categoryToAdd == null)
                {
                    return this.BadRequest();
                }
                return this.Ok(categoryToAdd);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error adding data to the database!");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategory(Category category, int id)
        {
            try
            {
                if (id != category.Id)
                {
                    return this.NotFound("Category id mismatch!");
                }

                var categoryToUpdate = await this.CategoryService.GetCategory(id);

                if (categoryToUpdate == null)
                {
                    return this.NotFound($"Category with an ID of {id} was not found!");
                }

                var result = this.CategoryService.UpdateCategory(category);
                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error updating data!");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                var categoryToDelete = await this.CategoryService.GetCategory(id);

                if (categoryToDelete == null)
                {
                    return this.NoContent();
                }

                var result = this.CategoryService.DeleteCategory(id);
                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data from the database!");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory(int id)
        {
            try
            {
                var result = await this.CategoryService.GetCategory(id);

                if (result == null)
                {
                    return this.NotFound();
                }

                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error getting data from the database!");
            }
        }

        [HttpPost("{id}/items")]
        public async Task<IActionResult> AddItemInCategory(int id,[FromBody] List<Item> item)
        {
            try
            {
                var category = await this.CategoryService.GetCategory(id);

                if (category == null)
                {
                    return this.NoContent();
                }

                var result = await this.CategoryService.AddItemInCategory(id, item);
                return this.Ok(result);
            }
            catch (ArgumentException)
            {
                return this.BadRequest();
            }
        }
    }
}
