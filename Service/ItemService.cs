﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository ItemRepository;
        public ItemService(IItemRepository itemRepository)
        {
            this.ItemRepository = itemRepository;
        }


        public async Task<Item> AddItem(Item item)
        {
            var itemList = await this.ItemRepository.GetAllItems();
            var itemToAdd = await this.ItemRepository.GetAnItem(item.Id);
            
            if (itemList.Contains(itemToAdd))
            {
                throw new DuplicateWaitObjectException(nameof(itemToAdd));
            }
            else if (itemToAdd.Name.Equals(" ") || itemToAdd.Name.Equals(""))
            {
                throw new ArgumentException("Name is empty!");
            }
            else if (itemToAdd.Description.Equals(" ") || itemToAdd.Description.Equals(""))
            {
                throw new ArgumentException("Description is empty!");
            }
            else if (itemToAdd.Price < 0)
            {
                throw new ArgumentException("Price is negative!");
            }
            else if (itemToAdd.Stock < 0)
            {
                throw new ArgumentException("Stock is negative!");
            }
            else if (itemToAdd.Calories < 0)
            {
                throw new ArgumentException("Calories is negative!");
            }

            var result = await this.ItemRepository.AddItem(item); 
            return result;
        }

        public async Task<Item> DeleteItem(int id)
        {
            var result = await this.ItemRepository.DeleteItem(id);
            return result;
        }

        public async Task<List<Item>> GetAllItems()
        {
            var items = await this.ItemRepository.GetAllItems();
            return items;
        }

        public async Task<Item> GetAnItem(int id)
        {
            var item = await this.ItemRepository.GetAnItem(id);

            if (item == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            return item;
        }

        public async Task<Item> UpdateItem(Item item)
        {
            var currentItem = await this.ItemRepository.GetAnItem(item.Id);
            var itemList = await this.ItemRepository.GetAllItems();

            if (!itemList.Contains(item))
            {
                throw new ArgumentOutOfRangeException("The item youre trying to update does not exist");
            }
            else if (currentItem == null)
            {
                throw new ArgumentNullException(nameof(currentItem));
            }

            var result = await this.ItemRepository.UpdateItem(item);
            return result;
        }
    }
}
