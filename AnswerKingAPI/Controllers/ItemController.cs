﻿using Domain;
using Service;
using Microsoft.AspNetCore.Mvc;

namespace AnswerKingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IItemService ItemService;
        public ItemController(IItemService itemService)
        {
            this.ItemService = itemService;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllItems()
        {
            try
            {
                var items = await this.ItemService.GetAllItems();

                return this.Ok(items);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error getting data from the database!");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddItem(Item item)
        {
            try
            {
                var result = await this.ItemService.AddItem(item);

                if (result == null)
                {
                    return this.BadRequest();
                }

                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error adding data to the database!");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateItem(Item item, int id)
        {
            try
            {
                if (id != item.Id)
                {
                    return this.NotFound("Item id mismatch!");
                }

                var itemToUpdate = await this.ItemService.GetAnItem(id);

                if (itemToUpdate == null)
                {
                    return this.BadRequest();
                }

                var result = await this.ItemService.UpdateItem(item);
                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error updating data!");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(int id)
        {
            try
            {
                var itemToDelete = await this.ItemService.GetAnItem(id);

                if (itemToDelete == null)
                {
                    return this.NoContent(); 
                }

                var result = ItemService.DeleteItem(id);
                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data from the database!");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAnItem(int id)
        {
            try
            {
                var result = await this.ItemService.GetAnItem(id);

                if (result == null)
                {
                    return this.NotFound();
                }

                return this.Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Error getting data from the database!");
            }
            
        }
    }
}
