﻿namespace AnswerKing
{
    public class Category
    {
        private int ID;
        private string Name;

        public Category(int ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }

        public string name
        {
            get { return Name; }
            set { Name = value; }
        }
    }
}
