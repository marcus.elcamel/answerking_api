﻿using Domain;
using Service;

namespace Infrastructure
{
    public class CategoryRepository : ICategoryRepository
    {
        //mock data
        public static List<Item> items = new List<Item>();

        public static List<Category> categories = new List<Category>()
        {
            new Category { Id = 1, Name = "Breakfast", Items = items},
            new Category { Id = 2, Name = "Dessert", Items = items}
        };

        public async Task<Category> AddCategory(Category category)
        {
            if (category == null)
            {
                throw new ArgumentNullException("Category is empty!");
            }
            categories.Add(category);
            return category;
        }

        public async Task<Category> DeleteCategory(int id)
        {
            var category = categories.SingleOrDefault(i => i.Id == id);

            if (category == null)
            {
                throw new ArgumentNullException($"Category with the id of {id} does not exist!");
            }
            categories.Remove(category);

            return category;
        }

        public async Task<List<Category>> GetAllCategories()
        {
            return categories;
        }

        public async Task<Category> GetCategory(int id)
        {
            var category = categories.SingleOrDefault(i => i.Id == id);
            return category;
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            var categoryToUpdate = categories.SingleOrDefault(i => i.Id == category.Id);

            if (categoryToUpdate == null)
            {
                throw new ArgumentNullException("Category is empty!");
            }

            categoryToUpdate.Id = category.Id;
            categoryToUpdate.Name = category.Name;

            return categoryToUpdate;
        }

        public async Task<Category> AddItemToCategory(int id, Item item)
        {
            var category = categories.SingleOrDefault(i => i.Id == id);

            if (item == null)
            {
                throw new ArgumentNullException("Item is empty!");
            }
            if (category == null)
            {
                throw new ArgumentNullException($"Category with the id of {id} does not exist!");
            }
            category.Items.Add(item);
            return category;
        }
  
    }
}
