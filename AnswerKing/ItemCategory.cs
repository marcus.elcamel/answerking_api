﻿namespace AnswerKing
{
    public class ItemCategory
    {
        private Item ItemID;
        private Category CategoryID;

        public ItemCategory(Item ItemID, Category CategoryID)
        {
            this.ItemID = ItemID;
            this.CategoryID = CategoryID;
        }

        public int itemID
        {
            get { return ItemID.id; }
            set { ItemID.id = value; }
        }
        public int categoryID
        {
            get { return CategoryID.id; }
            set { CategoryID.id = value; }
        }
    }
}
