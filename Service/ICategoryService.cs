﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ICategoryService
    {
        Task<Category> AddCategory(Category category);
        Task<List<Category>> GetAllCategories();
        Task<Category> GetCategory(int id);
        Task<Category> UpdateCategory(Category category);
        Task<Category> DeleteCategory(int id);
        Task<Category> AddItemInCategory(int id, List<Item> item);
    }
}
