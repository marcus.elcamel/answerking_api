﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<Item> Items { get; set; }

        public Category(int id, string name, List<Item> items)
        {
            this.Id = id;   
            this.Name = name;
            this.Items = items;
        }
        public Category()
        {

        }
    }
}
