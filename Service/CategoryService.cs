﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository CategoryRepository;

        private readonly IItemRepository ItemRepository;

        public CategoryService(ICategoryRepository categoryRepository, IItemRepository itemRepository)
        {
            this.CategoryRepository = categoryRepository;
            this.ItemRepository = itemRepository;
        }


        public async Task<Category> AddCategory(Category category)
        {
            var result = await this.CategoryRepository.AddCategory(category);
            return result;
        }

        public async Task<Category> DeleteCategory(int id)
        {
            var result = await this.CategoryRepository.DeleteCategory(id);
            return result;
        }

        public async Task<List<Category>> GetAllCategories()
        {
            var categories = await this.CategoryRepository.GetAllCategories();
            return categories;
        }

        public async Task<Category> GetCategory(int id)
        {
            var result = await this.CategoryRepository.GetCategory(id);
            return result;
        }

        public async Task<Category> UpdateCategory(Category category)
        {
            var currentCategory = await this.CategoryRepository.GetCategory(category.Id);

            if (currentCategory == null)
            {
                throw new ArgumentNullException(nameof(currentCategory));
            }
            var result = await this.CategoryRepository.UpdateCategory(category);
            return result;
        }

        public async Task<Category> AddItemInCategory(int id, List<Item> items)
        {
            var chosenCategory = await this.CategoryRepository.GetCategory(id);

            foreach (Item itemToAdd in items)
            {
                var currentItem = await this.ItemRepository.GetAnItem(itemToAdd.Id);

                if (currentItem == null)
                {
                    throw new NullReferenceException("The item you're trying to add does not exist!");
                }
                else if (chosenCategory.Items.Contains(currentItem))
                {
                    throw new ArgumentNullException("The item already exists in the category!");
                }
                await this.CategoryRepository.AddItemToCategory(id, currentItem);
            }
            return chosenCategory;
        }
    }
}
